#pragma once

#include <QDate>
#include <QString>
#include <QFile>

struct License
{
    static int GetId() { return _id; }
    static QString GetName() { return _name; }
    static QDate GetDate() { return _date; }

    static bool Load(QFile& qfile);
    static bool Save(QFile& qfile, const QString& name, const QDate& date);
    static void Create(int id);

private:

    static int _id;
    static QString _name;
    static QDate _date;

    License();
};
