#pragma once

#include <QString>

struct Crypter
{
    static QString TextEncrypt(const QString& plain_text_string);
    static QString TextDecrypt(const QString& encrypted_hex_string);

private:

    Crypter();
};
