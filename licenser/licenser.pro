#-------------------------------------------------
#
# Project created by QtCreator 2016-08-10T17:04:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = licenser
TEMPLATE = app


SOURCES += aes.c\
        main.cpp\
        mainwindow.cpp \
    license.cpp \
    crypter.cpp


HEADERS  += mainwindow.h \
    aes.h \
    license.h \
    crypter.h

FORMS    += mainwindow.ui

CONFIG += c++11
