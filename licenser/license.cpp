#include "license.h"

#include <QJsonObject>
#include <QJsonDocument>

#include "crypter.h"

int License::_id = 0;
QString License::_name;
QDate License::_date;

bool License::Save(QFile& qfile, const QString& name, const QDate& date)
{
    _name = name;
    _date = date;

    // create JSON + encode + save
    QJsonObject jsonLicenseObject
    {
        {"license_id", Crypter::TextEncrypt(QString::number(_id))},
        {"organization", Crypter::TextEncrypt(_name)},
        {"valid_through", Crypter::TextEncrypt(_date.toString("dd-MM-yyyy"))}
    };

    qfile.resize(0);
    qfile.write(QJsonDocument(jsonLicenseObject).toJson());
    return qfile.flush();
}

bool License::Load(QFile& qfile)
{
    // load data + parse JSON + decode
    QByteArray json_data = qfile.readAll();
    if (!json_data.size())
        return false;

    QJsonObject jsonLicenseObject = QJsonDocument(QJsonDocument::fromJson(json_data)).object();

    _id = Crypter::TextDecrypt( jsonLicenseObject["license_id"].toString() ).toInt();
    _name = Crypter::TextDecrypt( jsonLicenseObject["organization"].toString() );
    _date = QDate::fromString( Crypter::TextDecrypt( jsonLicenseObject["valid_through"].toString() ), "dd-MM-yyyy" );

    return true;
}

void License::Create(int id)
{
    _id = id;
    _name.clear();
    _date = QDate::currentDate();
}
