#include "mainwindow.h"
#include <QApplication>
#include <QString>
#include <QFile>
#include <QtDebug>

#include "license.h"

int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        // start GUI app
        QApplication app(argc, argv);
        MainWindow w;
        w.show();

        return app.exec();
    }

    // parse command-line parameters

    qInfo("License editor");

    QFile file(argv[1]);
    if (!file.open(QIODevice::ReadOnly)
            || !License::Load(file))
    {
        qDebug("\nUsage: licenser.exe [license_file.ljson]");
        return 0;
    }

    qInfo()
            << "\nLicense information:\nID: " << License::GetId()
            << "\nOrganization name: " << License::GetName()
            << "\nExpiry date: " << License::GetDate().toString("dd-MM-yyyy");

    return 0;
}
