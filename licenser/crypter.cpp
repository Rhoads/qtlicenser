#include "crypter.h"

#include "aes.h"

// CONSTANTS
const uint8_t AES128_KEY[] = { 11, 22, 33, 44, 55, 66, 77, 88, 11, 22, 33, 44, 55, 66, 77, 88, };
const uint8_t AES128_IV[] = { 55, 66, 77, 88, 11, 22, 33, 44, 55, 66, 77, 88, 11, 22, 33, 44, };

QString Crypter::TextEncrypt(const QString& plain_text_string)
{
    QByteArray plain_text_data((char*)plain_text_string.constData(), plain_text_string.size()*2); // copy

    if (int buf_tail_len = plain_text_data.size() % 16) // AES 16 byte align (zero filled)
    {
        buf_tail_len = 16 - buf_tail_len;
        plain_text_data.resize(plain_text_data.size() + buf_tail_len);
        memset(plain_text_data.data()+plain_text_data.size()-buf_tail_len, 0, buf_tail_len);
    }

    QByteArray encoded_data(plain_text_data.size(), 0);
    AES128_CBC_encrypt_buffer((uint8_t*)encoded_data.data(), (uint8_t*)plain_text_data.data(), plain_text_data.size(), AES128_KEY, AES128_IV);

    return encoded_data.toHex();
}

QString Crypter::TextDecrypt(const QString& encrypted_hex_string)
{
    QByteArray encrypted_hex_data((char*)encrypted_hex_string.constData(), encrypted_hex_string.size()*2);
    QByteArray encrypted_data = QByteArray::fromHex(encrypted_hex_data);
    QByteArray decrypted_data(encrypted_data.size(), 0);

    AES128_CBC_decrypt_buffer((uint8_t*)decrypted_data.data(), (uint8_t*)encrypted_data.data(), encrypted_data.size(), AES128_KEY, AES128_IV);

    // strip trailing zeroes
    for(int i=decrypted_data.size(); --i >= 0;)
        if (decrypted_data[i]) {
            decrypted_data.truncate(i+2);
            break;
        }

    return QString((QChar*)decrypted_data.constData(), decrypted_data.size()/2);
}
