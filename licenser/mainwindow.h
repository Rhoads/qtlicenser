#pragma once

#include <QMainWindow>
#include <QFile>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_menu_File_Exit_triggered();

    void on_menu_File_Open_triggered();

    void on_menu_File_New_triggered();

    void on_menu_File_Save_triggered();

private:
    Ui::MainWindow *ui;

    QFile _file_license;
};

