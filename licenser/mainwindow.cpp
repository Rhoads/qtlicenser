#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>

#include "license.h"

// CONSTANTS
const QString APP_NAME("Редактор лицензий");

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_menu_File_Exit_triggered()
{
    qApp->quit();
}

void MainWindow::on_menu_File_Open_triggered()
{
    QSettings settings("QuickQ", "License editor");
    QString fileName = QFileDialog::getOpenFileName(this, "Выбор лицензии", settings.value("last_save_dir").toString(), "Файлы лицензий (*.ljson)");

    if (fileName.isEmpty())
        return;

    if (_file_license.isOpen())
        _file_license.close();
    _file_license.setFileName(fileName);

    if (!_file_license.open(QIODevice::ReadWrite)
        || !License::Load(_file_license))
    {
        QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
        return;
    }

    ui->editLicenseNum->setText( QString::number( License::GetId() ) );
    ui->editOrganizationName->setText( License::GetName() );
    ui->editExpiryDate->setDate( License::GetDate() );

    ui->editOrganizationName->setEnabled(true);
    ui->editExpiryDate->setEnabled(true);
    ui->menu_File_Save->setEnabled(true);

    this->setWindowTitle(APP_NAME + " - " + QFileInfo(fileName).fileName());
}

void MainWindow::on_menu_File_New_triggered()
{
    if (_file_license.isOpen())
        _file_license.close();

    QSettings settings("QuickQ", "License editor");
    int intNewLicNum = settings.value("last_license").toInt() + 1;
    settings.setValue("last_license", intNewLicNum);

    License::Create(intNewLicNum);

    QString strLicenseNum(QString::number( intNewLicNum ));
    ui->editLicenseNum->setText( strLicenseNum );

    ui->editOrganizationName->setEnabled(true);
    ui->menu_File_Save->setEnabled(true);
    ui->editExpiryDate->setEnabled(true);

    ui->editExpiryDate->setDate( QDate().currentDate().addYears(1) );

    this->setWindowTitle(QString(APP_NAME + " - *"));

    ui->editOrganizationName->setFocus();
}

void MainWindow::on_menu_File_Save_triggered()
{
    if (!_file_license.isOpen())
    {
        QString fileName = QFileDialog::getSaveFileName(this, "Имя файла лицензии", QString(), "Файлы лицензий (*.ljson)");

        if (fileName.isEmpty())
            return;

        _file_license.setFileName(fileName);

        if (!_file_license.open(QIODevice::WriteOnly))
        {
            QMessageBox::critical(this, tr("Error"), tr("Could not save file"));
            return;
        }

        this->setWindowTitle(APP_NAME + " - " + QFileInfo(fileName).fileName());

        // save last dir
        QSettings settings("QuickQ", "License editor");
        settings.setValue("last_save_dir", QFileInfo(fileName).absolutePath());
    }

    if (License::Save(_file_license, ui->editOrganizationName->text(), ui->editExpiryDate->date()))
        return;

    QMessageBox::critical(this, tr("Error"), tr("Could not save file"));
}
